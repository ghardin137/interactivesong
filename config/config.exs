# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :interactivesong,
  ecto_repos: [Interactivesong.Repo]

# Configures the endpoint
config :interactivesong, InteractivesongWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Wpjd17DScBcx3XS3w19WC7M13USl8fS5ajCBw5ubndC5wZcZtRXuTuCqLnoWHAwB",
  render_errors: [view: InteractivesongWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Interactivesong.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Guardian
config :interactivesong, Interactivesong.Guardian,
  issuer: Interactivesong,
  module: Interactivesong.Guardian,
  error_handler: Interactivesong.AuthErrorHandler,
  ttl: {30, :days},
  allowed_drift: 2000,
  token_module: Guardian.Token.Jwt,
  secret_key: "mNWy/rwh9CyhSVLIor0VFYXSzLq1tqtPG2Qm5IX3Ayqh1jnLfvSah/TiX5fCGLyC"

config :guardian, Guardian.DB,
  repo: Interactivesong.Repo,
  # default
  schema_name: "guardian_tokens",
  # default: 60 minutes
  sweep_interval: 60

config :absinthe, Absinthe.Logger, filter_variables: ["token", "password", "confirm", "secret"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
