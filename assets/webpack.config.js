const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');

module.exports = () => {
    return {
        entry: './js/app.js',
        output: {
            path: path.resolve(__dirname, '../priv/static'),
            filename: 'js/app.js',
            chunkFilename: 'js/[name].js',
        },
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: path.resolve(__dirname, 'js'),
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: ['@babel/plugin-proposal-class-properties', ['emotion', { sourceMap: true }]],
                    },
                },
                {
                    test: /\.(s)?css$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                // you can specify a publicPath here
                                // by default it uses publicPath in webpackOptions.output
                                publicPath: '../',
                                hmr: process.env.NODE_ENV === 'development',
                            },
                        },
                        'css-loader',
                    ],
                },
                {
                    test: /\.graphql$/,
                    exclude: /node_modules/,
                    loader: 'graphql-tag/loader',
                },
            ],
        },
        resolve: {
            modules: ['node_modules', __dirname + '/js'],
            alias: {
                Queries: path.resolve(__dirname, 'js/queries'),
                Mutations: path.resolve(__dirname, 'js/mutations'),
                Components: path.resolve(__dirname, 'js/components'),
                Contexts: path.resolve(__dirname, 'js/contexts'),
            },
        },
        plugins: [
            new MiniCssExtractPlugin({ filename: 'css/app.css', chunkFilename: '[id].css' }),
            // new CopyWebpackPlugin([{ from: './assets' }]),
        ],
        optimization: {
            splitChunks: {
                chunks: 'initial',
            },
        },
    };
};
