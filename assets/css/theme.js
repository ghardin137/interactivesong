const theme = {
    padding: {
        small: 20,
        large: 40,
    },
    colors: {
        primary: '#2A2B33',
        secondary: '#AE5136',
        tertiary: '#879A9F',
        quatenary: '#E79B1E',
        white: '#F6F1F1',
        textColor: '#2A2B33',
        headingColor: '#2A2B33',
    },
    text: {
        heading: {
            face: "'Bree Serif', serif",
            sizes: {
                1: 30,
                2: 25,
                3: 21,
                4: 19,
                5: 17,
            },
        },
        copy: {
            face: "'Noto Sans', sans-serif",
            size: 14,
        },
    },
    buttons: {
        primary: {
            background: '#2A2B33',
            foreground: '#f6f1f1',
            border: '0 none',
        },
        success: {
            background: '#2A2B33',
            foreground: '#f6f1f1',
            border: '0 none',
        },
        danger: {
            background: '#2A2B33',
            foreground: '#f6f1f1',
            border: '0 none',
        },
        warning: {
            background: '#2A2B33',
            foreground: '#f6f1f1',
            border: '0 none',
        },
        link: {
            background: 'transparent',
            foreground: '#AE5136',
            border: '0 none',
        },
        default: {
            background: '#f6f1f1',
            foreground: '#2A2B33',
            border: '1px solid #2A2B33',
        },
        sizes: {
            sm: {
                padding: '3px 5px',
                fontSize: 12,
            },
            md: {
                padding: '7px 12px',
                fontSize: 14,
            },
            lg: {
                padding: '12px 18px',
                fontSize: 18,
            },
        },
    },
    breakpoints: {
        xs: 320,
        sm: 768,
        md: 1170,
        lg: 1366,
    },
};

export default theme;
