import gql from 'graphql-tag';

export const registerMutation = gql`
    mutation register($user: RegisterUserParams!) {
        register(user: $user) {
            id
            email
            name
            avatar
            __typename
        }
    }
`;
