import gql from 'graphql-tag';

export const phraseVoteMutation = gql`
    mutation phraseVote($phraseId: String!, $vote: PhraseVoteInput!) {
        voteOnPhrase(id: $phraseId, vote: $vote) {
            id
            volume
            panning
            inappropriate
            __typename
        }
    }
`;
