import gql from 'graphql-tag';
import { getCurrentSong } from 'Queries/getCurrentSong';

export const addTrackMutation = gql`
    mutation createSongPhrase($songPhrase: NewSongPhrase!) {
        createSongPhrase(songPhrase: $songPhrase) {
            id
            start
            end
            volume
            pan
            asset {
                id
                url
                timeSignatureBeats
                timeSignatureBasis
                instrument
                author
                __typename
            }
            __typename
        }
    }
`;

export const updateAfterAdd = (cache, { data: { createSongPhrase } }) => {
    const query = cache.readQuery({ query: getCurrentSong });
    const newPhrase = {
        id: createSongPhrase.id,
        start: createSongPhrase.start,
        end: createSongPhrase.end,
        volume: createSongPhrase.volume,
        pan: createSongPhrase.pan,
        __typename: createSongPhrase.__typename,
        asset: {
            ...createSongPhrase.asset,
        },
    };
    const newAsset = {
        ...createSongPhrase.asset,
        song_phrases: [newPhrase],
    };
    let exists = query.currentSong.assets.some(asset => asset.id === createSongPhrase.asset.id);
    let assets = query.currentSong.assets;
    if (!exists) assets = [...query.currentSong.assets, newAsset];
    else {
        assets = assets.map(asset => {
            if (asset.id === createSongPhrase.asset.id) {
                return {
                    ...asset,
                    song_phrases: [...asset.song_phrases, newPhrase],
                };
            }
            return asset;
        });
    }
    const phrases = query.currentSong.phrases.map((phrase, index) => {
        if (index === newPhrase.start) {
            return [...phrase, newPhrase];
        }
        return phrase;
    });
    const newQuery = {
        ...query,
        currentSong: {
            ...query.currentSong,
            phrases,
            assets,
        },
    };
    cache.writeQuery({
        query: getCurrentSong,
        data: newQuery,
    });
};
