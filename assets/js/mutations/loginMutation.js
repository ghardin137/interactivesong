import gql from 'graphql-tag';

export const loginMutation = gql`
    mutation login($login: LoginParams!) {
        login(login: $login) {
            id
            email
            name
            avatar
            roles {
                name
            }
            __typename
        }
    }
`;
