import gql from 'graphql-tag';
import { getCurrentSong } from 'Queries/getCurrentSong';

export const updateTrackPhraseMutation = gql`
    mutation updateTrackPhrase($songPhrase: NewSongPhrase!, $songPhraseId: String!) {
        updateSongPhrase(songPhrase: $songPhrase, songPhraseId: $songPhraseId) {
            id
            start
            end
            volume
            pan
            asset {
                id
                url
                timeSignatureBeats
                timeSignatureBasis
                instrument
                author
                __typename
            }
            __typename
        }
    }
`;

export const updateAfterAdd = (cache, { data: { createSongPhrase } }) => {
    const query = cache.readQuery({ query: getCurrentSong });
    const newPhrase = {
        id: createSongPhrase.id,
        start: createSongPhrase.start,
        end: createSongPhrase.end,
        volume: createSongPhrase.volume,
        pan: createSongPhrase.pan,
        __typename: createSongPhrase.__typename,
        asset: {
            id: createSongPhrase.asset.id,
            url: createSongPhrase.asset.url,
            instrument: createSongPhrase.asset.instrument,
            __typename: createSongPhrase.asset.__typename,
        },
    };
    let newAsset = query.currentSong.assets.find(asset => asset.id === createSongPhrase.asset.id);
    let assets = query.currentSong.assets;
    if (!newAsset) assets = [...query.currentSong.assets, newAsset];
    else {
        assets = assets.map(asset => {
            if (asset.id === createSongPhrase.asset.id) {
                return {
                    ...asset,
                    song_phrases: [...asset.song_phrases, newPhrase],
                };
            }
            return asset;
        });
    }
    const newQuery = {
        ...query,
        currentSong: {
            ...query.currentSong,
            phrases: [...query.currentSong.phrases, newPhrase],
            assets,
        },
    };
    cache.writeQuery({
        query: getCurrentSong,
        data: newQuery,
    });
};
