// @flow

import React, { useContext } from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import Login from './login';
import Profile from './profile';

import { UserContext } from 'Contexts/user-context';

const HeaderContainer = styled.header`
    flex-shrink: 0;
    display: flex;
    padding: 20px;
    background-color: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.white};

    h1 {
        margin-right: 50px;
    }
`;

const Nav = styled.nav`
    position: relative;
    display: flex;
    flex-grow: 1;
    ul {
        list-style-type: none;
        list-style-image: none;
        flex-grow: 1;
        display: flex;

        li {
            padding-left: 0;
        }
    }

    a {
        color: ${props => props.theme.colors.white};
        text-decoration: none;

        &:hover {
            color: ${props => props.theme.colors.quatenary};
        }
    }

    div {
        flex-shrink: 0;
    }
`;

const Header = () => {
    const user = useContext(UserContext);
    return (
        <HeaderContainer>
            <Nav>
                <ul>
                    <li>
                        <h1>Logo</h1>
                    </li>
                    <li>
                        <Link to="/play">Play</Link>
                    </li>
                </ul>
                <div>
                    {!user && <Login />}
                    {user && <Profile {...user} />}
                </div>
            </Nav>
        </HeaderContainer>
    );
};

export default Header;
