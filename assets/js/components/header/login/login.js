import React, { useState, useCallback } from 'react';
import styled from '@emotion/styled';
import { Mutation } from 'react-apollo';

import Input from 'Components/common/input';
import Button from 'Components/common/button';

import { userQuery } from 'Queries/getUserInfo';
import { loginMutation } from 'Mutations/loginMutation';

const LoginContainer = styled.div`
    position: absolute;
    padding: 20px;
    display: flex;
    flex-direction: column;
    right: 0;
    top: 30px;
    z-index: 10000;
    background: ${props => props.theme.colors.primary};
    box-shadow: ${props => props.theme.colors.primary}AA 0 5px 10px;
`;

const updateUserInfo = (cache, { data: { login } }) => {
    cache.writeQuery({
        query: userQuery,
        data: { me: login },
    });
};

const Login = () => {
    const [open, setOpen] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const openForm = useCallback(e => {
        if (e.preventDefault) e.preventDefault();
        if (e.stopPropagation) e.stopPropagation();
        setOpen(prev => !prev);
    }, []);
    const changeEmail = useCallback(e => {
        setEmail(e.target.value);
    }, []);
    const changePassword = useCallback(e => {
        setPassword(e.target.value);
    }, []);

    const clearForm = useCallback(() => {
        setEmail('');
        setPassword('');
        setOpen('');
    }, []);
    return (
        <Mutation mutation={loginMutation} update={updateUserInfo}>
            {(login, { loading, error }) => {
                return (
                    <>
                        <button onClick={openForm}>Login</button>
                        {open && (
                            <LoginContainer>
                                <Input label="Email" type="text" id="email" name="email" value={email} onChange={changeEmail} />
                                <Input label="Password" type="password" id="pass" name="pass" value={password} onChange={changePassword} />
                                <div>
                                    <Button variant="primary" onClick={() => login({ variables: { login: { email, password } } })}>
                                        Login
                                    </Button>
                                    <Button variant="link" onClick={clearForm}>
                                        Cancel
                                    </Button>
                                </div>
                            </LoginContainer>
                        )}
                    </>
                );
            }}
        </Mutation>
    );
};
export default Login;
