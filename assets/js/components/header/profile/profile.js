import React from 'react';
import styled from '@emotion/styled';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import Input from 'Components/common/input';
import Button from 'Components/common/button';

import { userQuery } from 'Queries/getUserInfo';
import { logoutMutation } from 'Mutations/logoutMutation';

const updateUserInfo = cache => {
    cache.writeQuery({
        query: userQuery,
        data: { me: null },
    });
};

const Profile = ({ email, avatar, name }) => {
    return (
        <Mutation mutation={logoutMutation} update={updateUserInfo} onCompleted={() => {}}>
            {(logout, { loading, error }) => {
                return (
                    <div>
                        {email}
                        <Button variant="link" onClick={() => logout()}>
                            Log out
                        </Button>
                    </div>
                );
            }}
        </Mutation>
    );
};
export default Profile;
