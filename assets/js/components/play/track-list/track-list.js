// @flow

import React, { useState, useContext } from 'react';
import styled from '@emotion/styled';

import { UserContext } from 'Contexts/user-context';

import AddTrack from 'Components/add-track';

const TrackListContainer = styled.ul`
    background-color: ${props => props.theme.primary};
    flex-shrink: 0;
    width: 120px;
    list-style-type: none;
    border-right: 2px solid #333;

    li {
        margin-left: 0;
        border-bottom: 1px solid #333;
        padding: 5px 0px;
        height: 86px;
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: center;
    }

    div {
        color: ${props => props.theme.colors.primary};
        padding-left: 10px;
        font-size: 16px;
        font-weight: bold;
    }

    button {
        padding-left: 10px;
        text-align: left;
        background: transparent;
        display: block;
        font-size: 16px;
        font-weight: bold;
        flex-grow: 1;
        border: 0;

        &:hover {
            background: ${props => props.theme.colors.primary};
            color: ${props => props.theme.colors.white};
        }
    }
`;

const TrackList = ({ assets }) => {
    const user = useContext(UserContext);
    return (
        <TrackListContainer>
            {assets.map(asset => {
                return (
                    <li key={asset.id}>
                        <div>{asset.instrument}</div>
                    </li>
                );
            })}
            {user && user.roles.some(role => role.name === 'Admin') && <AddTrack />}
        </TrackListContainer>
    );
};

export default TrackList;
