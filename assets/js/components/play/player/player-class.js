// @flow

import React, { Component } from 'react';
import styled from '@emotion/styled';

const PlayerContainer = styled.section`
    flex-shrink: 0;
    border-top: 1px solid black;
    padding: 20px;

    button {
        padding: 7px 12px;
        border-radius: 3px;
        color: ${props => props.theme.colors.white};
        background-color: ${props => props.theme.colors.tertiary};
        margin-right: 10px;
    }
`;

const audioCtx = new (window.AudioContext || window.webkitAudioContext)();

export default class Player extends Component {
    state = {
        audioLoading: true,
        audioPhrases: {},
        beat: 0,
    };

    timeout = null;
    playingNodes = [];

    componentDidMount() {
        this.loadAudio();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.assets !== this.props.assets) {
            this.loadAudio();
        }

        if (prevProps.playing !== this.props.playing) {
            this.play();
        }

        if (prevState.beat !== this.state.beat) {
            this.beat();
        }
    }

    componentWillUnmount() {
        if (this.timeout) clearInterval(this.timeout);
    }

    loadAudio = () => {
        const buffers = this.props.assets.map(asset => {
            if (asset.url) {
                return fetch(asset.url)
                    .then(res => res.arrayBuffer())
                    .then(buffer => audioCtx.decodeAudioData(buffer));
            }
            return Promise.resolve();
        });
        Promise.all(buffers).then(audio => {
            const audioBuffers = this.props.assets.reduce((result, current, index) => {
                if (!result[current.id]) result[current.id] = audio[index];
                return result;
            }, {});
            this.setState({ audioPhrases: audioBuffers, audioLoading: false });
        });
    };

    beat = () => {
        const { beat, audioPhrases } = this.state;
        const { playing, phrases, length } = this.props;

        if (playing) {
            this.stopPhrases(beat);
            if (beat > length) {
                this.props.stopPlaying();
            } else {
                if (phrases[beat] && phrases[beat].length > 0) {
                    phrases[beat].forEach(phrase => {
                        const audioCtx = new AudioContext({ latencyHint: 'interactive' });
                        const audio = audioCtx.createBufferSource();
                        audio.buffer = audioPhrases[phrase.asset.id];
                        const gain = audioCtx.createGain();
                        gain.gain.setValueAtTime(phrase.volume / 100, audioCtx.currentTime);
                        gain.connect(audioCtx.destination);
                        audio.connect(gain);
                        audio.start();
                        this.playingNodes.push({ ...phrase, node: audio });
                    });
                }
            }
        }
    };

    play = () => {
        if (this.props.playing) {
            this.timeout = setInterval(() => {
                this.setState(prev => ({ beat: prev.beat + 1 }));
            }, 60000 / this.props.tempo);
        } else {
            if (this.timeout) {
                clearInterval(this.timeout);
                this.setState({ beat: 0 });
            }
        }
    };

    stopPhrases = beat => {
        this.playingNodes = this.playingNodes.reduce((nodes, node) => {
            if (node.end <= beat) {
                node.node.stop();
            } else {
                nodes.push(node);
            }
            return nodes;
        }, []);
    };

    stop = () => {
        this.props.stopPlaying();
        this.stopPhrases(Number.MAX_SAFE_INTEGER);
    };

    render() {
        const { tempo, playing, length, togglePlaying } = this.props;
        const { audioLoading, beat } = this.state;
        return (
            <PlayerContainer>
                <button onClick={togglePlaying} disabled={audioLoading}>
                    {playing ? 'Pause' : 'Play'}
                </button>
                <button onClick={this.stop} disabled={audioLoading}>
                    Stop
                </button>

                <span style={{ fontWeight: 'bold', fontSize: 20 }}>
                    {beat * (60 / tempo)}/{length * (60 / tempo)}s
                </span>
            </PlayerContainer>
        );
    }
}
