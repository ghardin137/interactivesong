// @flow

import React, { useState, useEffect, useRef, useCallback } from 'react';
import styled from '@emotion/styled';

const PlayerContainer = styled.section`
    flex-shrink: 0;
    border-top: 1px solid black;
    padding: 20px;

    button {
        padding: 7px 12px;
        border-radius: 3px;
        color: ${props => props.theme.colors.white};
        background-color: ${props => props.theme.colors.tertiary};
        margin-right: 10px;
    }
`;

const audioCtx = new (window.AudioContext || window.webkitAudioContext)();

const Player = ({ tempo, phrases, assets, playing, reset, length, togglePlaying, stopPlaying }) => {
    const [audioLoading, setAudioLoading] = useState(true);
    const [audioPhrases, setAudioPhrases] = useState({});
    const [beat, setBeat] = useState(0);
    const timeout = useRef();
    const playingNodes = useRef([]);

    useEffect(() => {
        const buffers = assets.map(asset => {
            if (asset.url) {
                return fetch(asset.url)
                    .then(res => res.arrayBuffer())
                    .then(buffer => audioCtx.decodeAudioData(buffer));
            }
            return Promise.resolve();
        });
        Promise.all(buffers).then(audio => {
            const audioBuffers = assets.reduce((result, current, index) => {
                if (!result[current.id]) result[current.id] = audio[index];
                return result;
            }, {});
            setAudioPhrases(audioBuffers);
            setAudioLoading(false);
        });
    }, [assets]);

    const createTimeout = () => {
        return setTimeout(() => {
            setBeat(prev => prev + 1);
            timeout.current = createTimeout();
        }, 60000 / tempo);
    };

    useEffect(() => {
        if (playing) {
            if (beat !== 0) {
                playingNodes.current.forEach(node => node.context.resume());
            }
            timeout.current = createTimeout();
        } else {
            if (timeout.current) {
                clearTimeout(timeout.current);
            }
            if (reset) {
                setBeat(0);
            } else {
                playingNodes.current.forEach(node => node.context.suspend());
            }
        }
        return () => {
            if (timeout.current) clearTimeout(timeout.current);
        };
    }, [playing]);

    useEffect(() => {
        if (playing) {
            stopPhrases(beat);
            if (beat > length) {
                stop();
            } else {
                if (phrases[beat] && phrases[beat].length > 0) {
                    phrases[beat].forEach(phrase => {
                        const audioCtx = new AudioContext({ latencyHint: 'interactive' });
                        const audio = audioCtx.createBufferSource();
                        audio.buffer = audioPhrases[phrase.asset.id];
                        const gain = audioCtx.createGain();
                        gain.gain.setValueAtTime(phrase.volume / 100, audioCtx.currentTime);
                        gain.connect(audioCtx.destination);
                        audio.connect(gain);
                        audio.start();
                        playingNodes.current.push({ ...phrase, node: audio, context: audioCtx });
                    });
                }
            }
        }
    }, [beat, playing, audioPhrases]);

    const stopPhrases = beat => {
        playingNodes.current = playingNodes.current.reduce((nodes, node) => {
            console.log(node);
            if (node.end <= beat) {
                node.node.stop();
            } else {
                nodes.push(node);
            }
            return nodes;
        }, []);
    };

    const stop = useCallback(() => {
        stopPlaying();
        stopPhrases(Number.MAX_SAFE_INTEGER);
    }, []);

    return (
        <PlayerContainer>
            <button onClick={togglePlaying} disabled={audioLoading}>
                {playing ? 'Pause' : 'Play'}
            </button>
            <button onClick={stop} disabled={audioLoading}>
                Stop
            </button>

            <span style={{ fontWeight: 'bold', fontSize: 20 }}>
                {beat * (60 / tempo)}/{length * (60 / tempo)}s
            </span>
        </PlayerContainer>
    );
};

export default Player;
