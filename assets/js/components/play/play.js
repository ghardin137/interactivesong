import React, { useState, useCallback } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import styled from '@emotion/styled';

import Player from './player';
import SongRoll from './song-roll';

import { getCurrentSong } from 'Queries/getCurrentSong';

const Play = () => {
    const [playing, setPlaying] = useState(false);
    const [reset, setReset] = useState(false);

    const togglePlaying = useCallback(() => setPlaying(prev => !prev), []);
    const stopPlaying = useCallback(() => {
        setPlaying(false);
        setReset(true);
        setTimeout(() => setReset(false), 10);
    }, []);

    return (
        <Query query={getCurrentSong}>
            {({ loading, error, data }) => {
                if (loading) return <p>Loading...</p>;
                if (error) return <p>Error :(</p>;
                const { currentSong } = data;
                return (
                    <>
                        <SongRoll
                            currentSongId={currentSong.id}
                            assets={currentSong.assets}
                            playing={playing}
                            reset={reset}
                            length={currentSong.length + currentSong.tempo / 2}
                            tempo={currentSong.tempo}
                        />
                        <Player
                            phrases={currentSong.phrases}
                            assets={currentSong.assets}
                            tempo={currentSong.tempo}
                            length={currentSong.length}
                            playing={playing}
                            reset={reset}
                            togglePlaying={togglePlaying}
                            stopPlaying={stopPlaying}
                        />
                    </>
                );
            }}
        </Query>
    );
};

export default Play;
