// @flow

import React, { useContext, useState, useCallback } from 'react';
import styled from '@emotion/styled';
import { graphql } from 'react-apollo';

// Contexts
import { UserContext } from 'Contexts/user-context';

// Queries and Mutations
import { phraseVoteMutation } from 'Mutations/phraseVoteMutation';

// Components
import VolumeDown from 'Components/common/icons/volume-down.js';
import VolumeUp from 'Components/common/icons/volume-up.js';

const VoterContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.2);
    sbackground-blend-mode: screen;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 10px;
`;

const VotePanel = ({ phraseId, mutate }) => {
    const user = useContext(UserContext);
    const [volume, setVolume] = useState(0);
    const [pan, setPan] = useState(0);
    const [inappropriate, setInappropriate] = useState(false);

    const setVolumeUp = useCallback(e => {
        e.stopPropagation();
        setVolume(prev => {
            if (prev) return 0;
            return 1;
        });
        mutate({ variables: { phraseId, vote: { volume: volume ? 0 : 1, pan, inappropriate } } });
    }, []);
    const setVolumeDown = useCallback(e => {
        e.stopPropagation();
        setVolume(prev => {
            if (prev) return 0;
            return -1;
        });
        mutate({ variables: { phraseId, vote: { volume: volume ? 0 : -1, pan, inappropriate } } });
    }, []);

    return (
        <VoterContainer>
            {!!user ? (
                <>
                    <span onClick={setVolumeUp}>
                        <VolumeUp color={volume === 1 ? '#ffffff' : null} />
                    </span>
                    <span onClick={setVolumeDown}>
                        <VolumeDown color={volume === -1 ? '#ffffff' : null} />
                    </span>
                </>
            ) : (
                <p>You must LOGIN to vote</p>
            )}
        </VoterContainer>
    );
};

export default graphql(phraseVoteMutation)(VotePanel);
