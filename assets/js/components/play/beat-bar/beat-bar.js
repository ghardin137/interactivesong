// @flow

import React, { useEffect, useCallback } from 'react';
import styled from '@emotion/styled';
import { Mutation } from 'react-apollo';
import { useDrop } from 'react-dnd';

import { addTrackMutation, updateAfterAdd } from 'Mutations/addTrackMutation';

const BeatBarContainer = styled.div`
    grid-column-start: ${props => props.beat + 1};
    grid-column-end: ${props => props.beat + 2};
    grid-row: 1;
    background-color: ${props => (props.isOver ? props.theme.colors.quatenary : props.theme.colors.white)};
    border: 1px solid ${props => props.theme.colors.secondary};
    font-size: 16px;
    font-weight: bold;
    position: relative;
`;

const BeatBar = ({ beat, asset, currentSongId }) => {
    const [{ isOver }, drop] = useDrop({
        accept: asset.id,
        drop: ({ id }) => console.log(id),
        collect: monitor => ({ isOver: monitor.isOver() }),
    });

    return (
        <Mutation mutation={addTrackMutation} update={updateAfterAdd}>
            {createSongPhrase => (
                <BeatBarContainer
                    ref={drop}
                    beat={beat}
                    isOver={isOver}
                    onClick={() =>
                        createSongPhrase({
                            variables: {
                                songPhrase: {
                                    assetId: asset.id,
                                    start: beat,
                                    end: beat + asset.bars,
                                    name: asset.instrument,
                                    pan: 0,
                                    volume: 100,
                                    songId: currentSongId,
                                },
                            },
                        })
                    }>
                    <span dangerouslySetInnerHTML={{ __html: '&nbsp;' }} />
                </BeatBarContainer>
            )}
        </Mutation>
    );
};

export default BeatBar;
