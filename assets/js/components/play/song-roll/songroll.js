// @flow

import React from 'react';
import styled from '@emotion/styled';
import { keyframes } from '@emotion/core';
import HTML5Backend from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

import SongTrack from 'Components/play/song-track';
import TrackList from 'Components/play/track-list';

const SongRollContainer = styled.section`
    flex-grow: 1;
    overflow-y: auto;
    overflow-x: hidden;
`;

const scroll = keyframes`
    from {
        transform: translateX(0);
    }
    to {
        transform: translateX(-100%);
    }
`;

const SongRollAnimator = styled.div`
    width: ${props => props.length * 20}px;
    animation: ${props => (props.reset ? '' : scroll)} linear ${props => props.duration}s;
    animation-play-state: ${props => (props.playing ? 'running' : 'paused')};
`;

const TrackContainer = styled.div`
    flex-grow: 1;
    display: flex;
`;

const SongRoll = ({ playing, reset, assets, length, tempo, currentSongId }) => {
    return (
        <DndProvider backend={HTML5Backend}>
            <TrackContainer>
                <TrackList assets={assets} />
                <SongRollContainer>
                    <SongRollAnimator playing={playing} length={length} duration={length * (60 / tempo)} reset={reset}>
                        {assets.map(asset => (
                            <SongTrack key={asset.id} length={length} asset={asset} currentSongId={currentSongId} />
                        ))}
                    </SongRollAnimator>
                </SongRollContainer>
            </TrackContainer>
        </DndProvider>
    );
};

export default SongRoll;
