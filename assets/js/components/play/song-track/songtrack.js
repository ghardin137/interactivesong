// @flow

import React, { useContext, useMemo } from 'react';
import styled from '@emotion/styled';
import { UserContext } from 'Contexts/user-context';

import PhraseBar from 'Components/play/phrase-bar';
import BeatBar from 'Components/play/beat-bar';

const SongTrackContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(${props => props.length}, 20px);
    grid-template-rows: 75px;
    border-bottom: 1px dashed ${props => props.theme.colors.primary};
    padding: 5px 0;
`;

const SongTrack = ({ asset, length, currentSongId }) => {
    const user = useContext(UserContext);
    const beats = useMemo(() => Array.from({ length }), [length]);
    return (
        <SongTrackContainer key={asset.id} length={length}>
            {user &&
                user.roles.some(role => role.name === 'Admin') &&
                beats.map((_, index) => <BeatBar key={`${asset.id}-${index}`} beat={index} asset={asset} currentSongId={currentSongId} />)}
            {asset.song_phrases.map(phrase => (
                <PhraseBar key={phrase.id} phrase={phrase} asset={asset} />
            ))}
        </SongTrackContainer>
    );
};

export default SongTrack;
