// @flow

import React, { useState, useCallback, useEffect } from 'react';
import styled from '@emotion/styled';
import { useDrag } from 'react-dnd';

import VotePanel from 'Components/play/vote-panel';

const PhraseBarContainer = styled.div`
    padding: 20px;
    grid-column-start: ${props => props.start + 1};
    grid-column-end: ${props => props.end + 1};
    grid-row: 1;
    background-color: ${props => props.theme.colors.quatenary};
    border: 1px solid ${props => props.theme.colors.secondary};
    color: ${props => props.theme.colors.primary};
    font-size: 16px;
    font-weight: bold;
    position: relative;
`;

const PhraseBar = ({ phrase, asset }) => {
    const [{ isDragging }, drag] = useDrag({
        item: { type: asset.id, id: phrase.id },
        collect: monitor => ({ isDragging: monitor.isDragging() }),
    });

    const [voterVisible, setVoterVisible] = useState(false);

    const showVoter = useCallback(event => {
        event.preventDefault();
        event.stopPropagation();
        setVoterVisible(prev => !prev);
    }, []);
    if (isDragging) return null;
    return (
        <PhraseBarContainer ref={drag} key={phrase.id} start={phrase.start} end={phrase.end} onClick={showVoter}>
            <span dangerouslySetInnerHTML={{ __html: '&nbsp;' }} />
            {voterVisible && <VotePanel phraseId={phrase.id} />}
        </PhraseBarContainer>
    );
};

export default PhraseBar;
