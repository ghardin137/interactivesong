// @flow

import React from "react";
import styled from "@emotion/styled";

const BodyContainer = styled.article`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;
export default BodyContainer;
