import React from 'react';
import styled from '@emotion/styled';

const Button = styled.button`
    background: ${props => props.theme.buttons[props.variant ? props.variant : 'default'].background};
    color: ${props => props.theme.buttons[props.variant ? props.variant : 'default'].foreground};
    border: ${props => props.theme.buttons[props.variant ? props.variant : 'default'].border};
    padding: ${props => props.theme.buttons.sizes[props.size ? props.size : 'md'].padding};
    font-size: ${props => props.theme.buttons.sizes[props.size ? props.size : 'md'].fontSize}px;
    cursor: pointer;
`;

export default Button;
