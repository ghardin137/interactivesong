import React, { useState, useCallback } from 'react';
import styled from '@emotion/styled';

const InputContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 1.2rem;
`;

const Input = ({ label, style, ...inputProps }) => {
    return (
        <InputContainer>
            <label htmlFor={inputProps.name}>{label}</label>
            <input {...inputProps} />
        </InputContainer>
    );
};

export default Input;
