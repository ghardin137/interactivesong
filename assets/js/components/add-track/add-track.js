// @flow

import React, { useState } from 'react';
import styled from '@emotion/styled';
import { Query, Mutation } from 'react-apollo';

import { addTrackMutation, updateAfterAdd } from 'Mutations/addTrackMutation';
import { getAvailableAssets } from 'Queries/getAvailableAssets';

const AddTrack = () => {
    const [addVisible, setVisible] = useState(false);
    const [asset_id, setAsset] = useState('');
    return (
        <li>
            <Mutation mutation={addTrackMutation} update={updateAfterAdd}>
                {createSongPhrase => (
                    <Query query={getAvailableAssets}>
                        {({ loading, error, data }) => {
                            return (
                                <>
                                    {!addVisible && <button onClick={() => setVisible(prev => !prev)}>Add Track</button>}
                                    {addVisible && (
                                        <>
                                            <select onChange={e => setAsset(e.target.value)} value={asset_id}>
                                                {data &&
                                                    data.allAssets.map(asset => (
                                                        <option key={asset.id} value={asset.id}>
                                                            {asset.instrument}
                                                        </option>
                                                    ))}
                                            </select>
                                            <button
                                                onClick={() => {
                                                    if (asset_id) {
                                                        const newAsset = data.allAssets.find(asset => asset.id === asset_id);
                                                        createSongPhrase({
                                                            variables: {
                                                                songPhrase: {
                                                                    assetId: asset_id,
                                                                    start: 0,
                                                                    end: newAsset.bars,
                                                                    name: newAsset.instrument,
                                                                    pan: 0,
                                                                    volume: 100,
                                                                    songId: data.currentSong.id,
                                                                },
                                                            },
                                                        });
                                                    }
                                                }}>
                                                Add Track
                                            </button>
                                        </>
                                    )}
                                </>
                            );
                        }}
                    </Query>
                )}
            </Mutation>
        </li>
    );
};

export default AddTrack;
