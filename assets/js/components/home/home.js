// @flow

import React from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div>
      <Link to="/play">Play</Link>
    </div>
  );
};

export default Home;
