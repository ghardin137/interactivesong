// @flow

import React, { useContext } from 'react';
import styled from '@emotion/styled';
import { Mutation } from 'react-apollo';

import { UserContext } from 'Contexts/user-context';

import Input from 'Components/common/input';
import Button from 'Components/common/button';

import { userQuery } from 'Queries/getUserInfo';
import { registerMutation } from 'Mutations/registerMutation';

const createMediaQuery = breakpoint => {
    const width = breakpoint < 1170 ? `${breakpoint}px` : '1170px';
    const size = breakpoint > 1170 ? 'max' : 'min';
    const query = `@media(${size}-width: ${breakpoint}px) {
        width: 100%;
        max-width: ${width};
        margin: 0 auto;
        box-sizing: border-box;
    }`;
    return query;
};

const RegisterForm = styled.div`
    ${props =>
        Object.values(props.theme.breakpoints)
            .map(createMediaQuery)
            .join('\n')}
    padding: ${props => props.theme.padding.large}px;

    h1 {
        font-size: ${props => props.theme.text.heading.sizes[1]}px;
        font-family: ${props => props.theme.text.heading.face};
    }
`;

const Register = ({ history }) => {
    const user = useContext(UserContext);
    if (user) history.replace('/');
    return (
        <RegisterForm>
            <h1>Register</h1>
        </RegisterForm>
    );
};

export default Register;
