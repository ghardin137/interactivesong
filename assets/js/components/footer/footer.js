// @flow

import React from "react";
import styled from "@emotion/styled";

const FooterContainer = styled.footer``;

const Footer = () => {
  return <FooterContainer />;
};

export default Footer;
