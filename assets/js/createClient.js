import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';

const createClient = (uri = '/api') => {
    const link = new HttpLink({
        uri,
        credentials: 'same-origin',
    });
    const cache = new InMemoryCache();

    return new ApolloClient({ link, cache });
};

export default createClient;
