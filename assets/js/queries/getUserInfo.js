import gql from 'graphql-tag';

export const userQuery = gql`
    query UserInfo {
        me {
            id
            email
            name
            avatar
            roles {
                name
            }
        }
    }
`;
