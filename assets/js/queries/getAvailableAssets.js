import gql from 'graphql-tag';

export const getAvailableAssets = gql`
    query getAvailableAssets {
        allAssets {
            id
            url
            timeSignatureBeats
            timeSignatureBasis
            instrument
            author
            bars
        }
        currentSong {
            id
        }
    }
`;
