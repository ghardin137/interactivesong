import gql from 'graphql-tag';

export const getCurrentSong = gql`
    query currentsong {
        currentSong {
            id
            name
            phrases {
                id
                start
                end
                volume
                pan
                asset {
                    id
                    url
                    instrument
                }
            }
            assets {
                id
                url
                timeSignatureBeats
                timeSignatureBasis
                instrument
                author
                bars
                song_phrases {
                    id
                    start
                    end
                    volume
                    pan
                }
            }
            tempo
            length
            averageScore
        }
    }
`;
