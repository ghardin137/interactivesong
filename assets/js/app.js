import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider, Query } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import styled from '@emotion/styled';
import theme from '../css/theme';
import app from '../css/app.css';

// Components
import Header from 'Components/header';
import Footer from 'Components/footer';
import BodyContainer from 'Components/common/body-container';
import Home from 'Components/home';
import Play from 'Components/play';
import Register from 'Components/register';

// Contexts
import { UserContext } from 'Contexts/user-context';

// Graphql Queries
import { userQuery } from 'Queries/getUserInfo';

import createClient from './createClient';

const client = createClient();

const createMediaQuery = breakpoint => {
    const width = breakpoint < 1170 ? `${breakpoint}px` : '100vw';
    const size = breakpoint > 1170 ? 'max' : 'min';
    const query = `@media(${size}-width: ${breakpoint}px) {
        max-width: ${width};
        margin: 0 auto;
        box-sizing: border-box;
    }`;
    return query;
};

const PageContainer = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    width: 100%;
    background-color: ${props => props.theme.colors.white};
    font-size: ${props => props.theme.text.copy.size}px;
    ${props =>
        Object.values(props.theme.breakpoints)
            .map(createMediaQuery)
            .join('\n')}
`;

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <ApolloProvider client={client}>
                <Router>
                    <Query query={userQuery} errorPolicy="ignore">
                        {({ loading, error, data }) => {
                            return (
                                <UserContext.Provider value={data ? data.me : null}>
                                    <PageContainer>
                                        <Header />
                                        <BodyContainer>
                                            <Route path="/" exact component={Home} />
                                            <Route path="/play" component={Play} />
                                            <Route path="/register" exact component={Register} />
                                        </BodyContainer>
                                        <Footer />
                                    </PageContainer>
                                </UserContext.Provider>
                            );
                        }}
                    </Query>
                </Router>
            </ApolloProvider>
        </ThemeProvider>
    );
};

ReactDOM.render(<App />, document.getElementById('root'));
