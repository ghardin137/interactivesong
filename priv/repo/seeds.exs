# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Interactivesong.Repo.insert!(%Interactivesong.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
import String

alias Interactivesong.Repo
alias Interactivesong.Role
alias Interactivesong.Permission
alias Interactivesong.User
alias Interactivesong.Asset
alias Interactivesong.SongPhrase
alias Interactivesong.Song

Repo.query("Truncate role_permissions", [])
Repo.query("Truncate user_roles", [])
Repo.delete_all(Permission)
Repo.delete_all(Role)
Repo.delete_all(User)
Repo.delete_all(SongPhrase)
Repo.delete_all(Asset)
Repo.delete_all(Song)

admin =
  %User{}
  |> User.registration_changeset(%{
    name: "Admin",
    email: "greg@virtuallite.com",
    password: "123456"
  })
  |> Repo.insert!()

adminRole = Repo.insert!(%Role{name: "Admin", active: true})
editorRole = Repo.insert!(%Role{name: "Editor", active: true})
voterRole = Repo.insert!(%Role{name: "Voter", active: true})

assets_create = Repo.insert!(%Permission{name: "assets.create", active: true})
assets_update = Repo.insert!(%Permission{name: "assets.update", active: true})
assets_remove = Repo.insert!(%Permission{name: "assets.remove", active: true})
song_create = Repo.insert!(%Permission{name: "song.create", active: true})
song_update = Repo.insert!(%Permission{name: "song.update", active: true})
song_remove = Repo.insert!(%Permission{name: "song.remove", active: true})
song_phrase_create = Repo.insert!(%Permission{name: "song_phrase.create", active: true})
song_phrase_update = Repo.insert!(%Permission{name: "song_phrase.update", active: true})
song_phrase_remove = Repo.insert!(%Permission{name: "song_phrase.remove", active: true})
phrase_vote_read = Repo.insert!(%Permission{name: "phrase_vote.read", active: true})
phrase_vote_create = Repo.insert!(%Permission{name: "phrase_vote.create", active: true})
phrase_vote_update = Repo.insert!(%Permission{name: "phrase_vote.update", active: true})
phrase_vote_remove = Repo.insert!(%Permission{name: "phrase_vote.remove", active: true})
vote_create = Repo.insert!(%Permission{name: "vote.create", active: true})
vote_update = Repo.insert!(%Permission{name: "vote.update", active: true})
vote_remove = Repo.insert!(%Permission{name: "vote.remove", active: true})
role_read = Repo.insert!(%Permission{name: "role.read", active: true})
role_create = Repo.insert!(%Permission{name: "role.create", active: true})
role_update = Repo.insert!(%Permission{name: "role.update", active: true})
role_remove = Repo.insert!(%Permission{name: "role.remove", active: true})
role_permission_create = Repo.insert!(%Permission{name: "role.permission.create", active: true})
role_permission_remove = Repo.insert!(%Permission{name: "role.permission.remove", active: true})
permission_read = Repo.insert!(%Permission{name: "permission.read", active: true})
permission_create = Repo.insert!(%Permission{name: "permission.create", active: true})
permission_update = Repo.insert!(%Permission{name: "permission.update", active: true})
permission_remove = Repo.insert!(%Permission{name: "permission.remove", active: true})
users_read = Repo.insert!(%Permission{name: "users.read", active: true})
user_create = Repo.insert!(%Permission{name: "user.create", active: true})
user_update = Repo.insert!(%Permission{name: "user.update", active: true})
user_remove = Repo.insert!(%Permission{name: "user.remove", active: true})
user_role_create = Repo.insert!(%Permission{name: "user.role.create", active: true})
user_role_remove = Repo.insert!(%Permission{name: "user.role.remove", active: true})

adminRole
|> Repo.preload(:permissions)
|> Role.add_perm_changeset(assets_create)
|> Repo.update!()
|> Role.add_perm_changeset(assets_remove)
|> Repo.update!()
|> Role.add_perm_changeset(song_create)
|> Repo.update!()
|> Role.add_perm_changeset(song_update)
|> Repo.update!()
|> Role.add_perm_changeset(song_remove)
|> Repo.update!()
|> Role.add_perm_changeset(song_phrase_remove)
|> Repo.update!()
|> Role.add_perm_changeset(phrase_vote_read)
|> Repo.update!()
|> Role.add_perm_changeset(phrase_vote_remove)
|> Repo.update!()
|> Role.add_perm_changeset(vote_remove)
|> Repo.update!()
|> Role.add_perm_changeset(role_read)
|> Repo.update!()
|> Role.add_perm_changeset(role_create)
|> Repo.update!()
|> Role.add_perm_changeset(role_update)
|> Repo.update!()
|> Role.add_perm_changeset(role_remove)
|> Repo.update!()
|> Role.add_perm_changeset(role_permission_create)
|> Repo.update!()
|> Role.add_perm_changeset(role_permission_remove)
|> Repo.update!()
|> Role.add_perm_changeset(permission_read)
|> Repo.update!()
|> Role.add_perm_changeset(permission_create)
|> Repo.update!()
|> Role.add_perm_changeset(permission_update)
|> Repo.update!()
|> Role.add_perm_changeset(permission_remove)
|> Repo.update!()
|> Role.add_perm_changeset(users_read)
|> Repo.update!()
|> Role.add_perm_changeset(user_create)
|> Repo.update!()
|> Role.add_perm_changeset(user_update)
|> Repo.update!()
|> Role.add_perm_changeset(user_remove)
|> Repo.update!()
|> Role.add_perm_changeset(user_role_create)
|> Repo.update!()
|> Role.add_perm_changeset(user_role_remove)
|> Repo.update!()

editorRole
|> Repo.preload(:permissions)
|> Role.add_perm_changeset(assets_update)
|> Repo.update!()
|> Role.add_perm_changeset(song_phrase_create)
|> Repo.update!()
|> Role.add_perm_changeset(song_phrase_update)
|> Repo.update!()

voterRole
|> Repo.preload(:permissions)
|> Role.add_perm_changeset(phrase_vote_create)
|> Repo.update!()
|> Role.add_perm_changeset(phrase_vote_update)
|> Repo.update!()
|> Role.add_perm_changeset(vote_create)
|> Repo.update!()
|> Role.add_perm_changeset(vote_update)
|> Repo.update!()

admin
|> Repo.preload(:roles)
|> User.add_role_changeset(adminRole)
|> Repo.update!()
|> User.add_role_changeset(editorRole)
|> Repo.update!()
|> User.add_role_changeset(voterRole)
|> Repo.update!()

{:ok, song} = Repo.insert(%Song{name: "Test Song", active: true, tempo: 120})

defmodule Seeder do
  def addAsset(url) do
    parts = split(slice(url, 38..-1), "/")

    style = Enum.at(parts, 0)
    instrument = replace(Enum.at(split(Enum.at(parts, 1), "."), 0), "_", " ")

    Repo.insert(%Asset{
      url: url,
      bars: 8,
      instrument: instrument,
      tempo: 120,
      author: style,
      time_signature_beats: 4,
      time_signature_basis: 4
    })
  end
end

Enum.each(
  [
    "https://penguin.virtuallite.com/audio/Ambient_Chill_Out/Beat.ogg",
    "https://penguin.virtuallite.com/audio/Ambient_Chill_Out/Kick.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/histle_Lead.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/olo_Lead.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/Pad.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_2.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_4.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_1.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_3.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Noise.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_5.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_3.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_4.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Synth_Arpegg_2.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Synth_Arpegg_1.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Synth.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Laser_Wob.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Mixed_Beat.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Blast.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Glass_Arp_2.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Beat.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Glass_Arp_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_3.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Bass_Drum_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Crash_Cymbal.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Bass_Drum_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Drum_Beat_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Drum_Beat_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Drum_Beat_3.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Drum_Fill_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Drum_Fill_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Chords_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Melody_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_End.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Chords_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Melody_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Riff_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Short_Melody_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Riff_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Short_Melody_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Electric_Guitar_Short_Melody_High.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Hi_Hat_Synth_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Hi_Hat_Synth_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Muted_Bass_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Muted_Bass_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Rimshot.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Saw_Synth_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Saw_Synth_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Saw_Synth_3.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Snare_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Snare_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Saw_Synth_End.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Synth_Element_1.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Synth_Element_2.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Synth_Element_3.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Synth_Effect_Ambience.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Saw_Synth_4.ogg",
    "https://penguin.virtuallite.com/audio/80s_Synth_Rock/Synth_Intro.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Caverns.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Atmosphere.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Digestion.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Haunting.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Dark_Space.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Landing.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Machines.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Pads_2.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Pads_1.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Sci_Fi_Synths.ogg",
    "https://penguin.virtuallite.com/audio/Ambient/Space.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Synth_2.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Synth_3.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Synth_4.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Break_1.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Atmosphere_Synth_1.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Click_Fall.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Break_2.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Deep_Chord_1.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Deep_Chord_3.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Lead_2.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Deep_Chord_2.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Lead_3.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Lead_4.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Lead_5.ogg",
    "https://penguin.virtuallite.com/audio/Breakbeat/Lead_1.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/Whistle_Lead.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/Solo_Lead.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Chill_Out/Pad.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_2.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_4.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_1.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_3.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Noise.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Arpeggiator_5.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_3.ogg",
    "https://penguin.virtuallite.com/audio/Atmospheric_Industrial_Breaks/Pad_4.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Synth_Arpegg_2.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Synth_Arpegg_1.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Ambient/Trance_Synth.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Laser_Wob.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Mixed_Beat.ogg",
    "https://penguin.virtuallite.com/audio/Cinematic_Dubstep/Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Blast.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Glass_Arp_2.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Beat.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_2.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Glass_Arp_1.ogg",
    "https://penguin.virtuallite.com/audio/BitWave/Scifi_Pad_3.ogg"
  ],
  fn url -> Seeder.addAsset(url) end
)

{:ok, asset} =
  Repo.insert(%Asset{
    url: "https://penguin.virtuallite.com/Extra_Sauce.mp3",
    bars: 8,
    instrument: "Sitar",
    tempo: 120,
    author: "bob",
    time_signature_beats: 4,
    time_signature_basis: 4
  })

Repo.insert(%SongPhrase{
  start: 0,
  end: 10,
  volume: 60,
  pan: 0,
  asset_id: asset.id,
  song_id: song.id
})

Repo.insert(%SongPhrase{
  start: 16,
  end: 32,
  volume: 100,
  pan: 0,
  asset_id: asset.id,
  song_id: song.id
})

Repo.insert(%SongPhrase{
  start: 35,
  end: 51,
  volume: 100,
  pan: 0,
  asset_id: asset.id,
  song_id: song.id
})

Repo.insert(%SongPhrase{
  start: 52,
  end: 68,
  volume: 100,
  pan: 0,
  asset_id: asset.id,
  song_id: song.id
})
