defmodule Interactivesong.Repo.Migrations.FixPhraseVoteTable do
  use Ecto.Migration

  def change do
    alter table(:phrase_votes, primary_key: false) do
      remove(:phrase_id)
      add(:song_phrase_id, references(:song_phrases, type: :uuid, on_delete: :nothing))
    end
  end
end
