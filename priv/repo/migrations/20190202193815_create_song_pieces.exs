defmodule Interactivesong.Repo.Migrations.CreateSongPieces do
  use Ecto.Migration

  def change do
    create table(:song_phrases, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:start, :integer)
      add(:end, :integer)
      add(:volume, :integer)
      add(:pan, :integer)

      add(:song_id, references(:songs, type: :uuid, on_delete: :nothing))
      add(:asset_id, references(:assets, type: :uuid, on_delete: :nothing))

      timestamps()
    end
  end
end
