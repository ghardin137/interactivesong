defmodule Interactivesong.Repo.Migrations.AddAuthorNoteToAsset do
  use Ecto.Migration

  def change do
    alter table(:assets, primary_key: false) do
      add(:author, :string)
    end
  end
end
