defmodule Interactivesong.Repo.Migrations.CreateSongModification do
  use Ecto.Migration

  def change do
    create table(:song_modification) do
      add(:volume, :integer)
      add(:pan, :integer)
      add(:start, :integer)
      add(:end, :integer)

      timestamps()

      add(:song_id, references(:songs, type: :uuid, on_delete: :nothing))
      add(:song_phrase_id, references(:song_phrases, type: :uuid, on_delete: :nothing))
    end
  end
end
