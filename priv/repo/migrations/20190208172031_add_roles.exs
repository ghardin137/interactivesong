defmodule Interactivesong.Repo.Migrations.AddRoles do
  use Ecto.Migration

  def change do
    create table(:roles, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:name, :string)
      add(:active, :boolean, default: false, null: false)
      timestamps()
    end

    create table(:permissions, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:name, :string)
      add(:active, :boolean, default: false, null: false)
      timestamps()
    end

    create table(:role_permissions, primary_key: false) do
      add(:role_id, references(:roles, type: :binary_id))
      add(:permission_id, references(:permissions, type: :binary_id))
      timestamps()
    end

    create table(:user_roles, primary_key: false) do
      add(:user_id, references(:users, type: :binary_id))
      add(:role_id, references(:roles, type: :binary_id))
      timestamps()
    end
  end
end
