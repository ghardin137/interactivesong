defmodule Interactivesong.Repo.Migrations.AddTimeSignature do
  use Ecto.Migration

  def change do
    alter table(:assets, primary_key: false) do
      add(:time_signature_beats, :integer, default: 4)
      add(:time_signature_basis, :integer, default: 4)
    end
  end
end
