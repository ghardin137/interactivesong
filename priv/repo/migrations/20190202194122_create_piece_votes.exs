defmodule Interactivesong.Repo.Migrations.CreatePieceVotes do
  use Ecto.Migration

  def change do
    create table(:phrase_votes, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:volume, :integer)
      add(:panning, :integer)
      add(:inappropriate, :boolean, default: false, null: false)

      add(:phrase_id, references(:song_phrases, type: :uuid, on_delete: :nothing))
      add(:user_id, references(:users, type: :uuid, on_delete: :nothing))

      timestamps()
    end
  end
end
