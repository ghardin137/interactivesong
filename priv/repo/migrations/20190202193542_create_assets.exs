defmodule Interactivesong.Repo.Migrations.CreateAssets do
  use Ecto.Migration

  def change do
    create table(:assets, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:url, :string)
      add(:bars, :integer)
      add(:instrument, :string)
      add(:tempo, :integer)

      timestamps()
    end
  end
end
