defmodule Interactivesong.Repo.Migrations.AddSongTempo do
  use Ecto.Migration

  def change do
    alter table(:songs, primary_key: false) do
      add(:tempo, :integer, default: 120)
    end
  end
end
