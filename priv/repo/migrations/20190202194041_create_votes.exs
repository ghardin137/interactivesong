defmodule Interactivesong.Repo.Migrations.CreateVotes do
  use Ecto.Migration

  def change do
    create table(:votes, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:level, :integer)

      add(:song_id, references(:songs, type: :uuid, on_delete: :nothing))
      add(:user_id, references(:users, type: :uuid, on_delete: :nothing))
      timestamps()
    end
  end
end
