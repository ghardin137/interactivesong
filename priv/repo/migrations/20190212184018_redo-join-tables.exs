defmodule :"Elixir.Interactivesong.Repo.Migrations.Redo-join-tables" do
  use Ecto.Migration

  def change do
    drop(table(:role_permissions))
    drop(table(:user_roles))

    create table(:role_permissions, primary_key: false) do
      add(:role_id, references(:roles, type: :binary_id))
      add(:permission_id, references(:permissions, type: :binary_id))
    end

    create table(:user_roles, primary_key: false) do
      add(:user_id, references(:users, type: :binary_id))
      add(:role_id, references(:roles, type: :binary_id))
    end
  end
end
