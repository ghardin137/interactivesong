# Interactive Song

To start the server:

-   Install dependencies with `mix deps.get`
-   Create and migrate your database with `mix ecto.create && mix ecto.migrate`
-   Seed the database with `mix run priv/repo/seeds.exs`
-   Install Node.js dependencies with `cd assets && npm install`
-   Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).
