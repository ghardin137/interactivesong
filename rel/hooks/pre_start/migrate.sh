#!/bin/sh

echo "Running migrations"
release_ctl eval --mfa "Interactivesong.ReleaseTasks.migrate/1" --argv -- "$@"
echo "Migrations run successfully"
