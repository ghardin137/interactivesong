#!/bin/sh

echo "Beginning Seeding"
release_ctl eval --mfa "Interactivesong.ReleaseTasks.seed/1" --argv -- "$@"
echo "Seeding Successful"