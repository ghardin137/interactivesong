defmodule Interactivesong.Context do
  @behaviour Plug
  alias Interactivesong.Repo
  alias Interactivesong.User

  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    case Guardian.Plug.current_resource(conn) do
      nil ->
        conn

      token_user ->
        token_user =
          case get_user_permissions(token_user.id) do
            roles ->
              Map.put(token_user, :roles, roles)
          end

        token = Guardian.Plug.current_token(conn)
        token_user = Map.put(token_user, :token, token)
        put_private(conn, :absinthe, %{context: %{current_user: token_user}})
    end
  end

  def get_user_permissions(id) do
    user =
      Repo.get(User, id)
      |> Repo.preload([{:roles, :permissions}])

    case user do
      nil ->
        []

      _ ->
        permissions =
          Enum.map(user.roles, fn role ->
            Enum.map(role.permissions, fn permission ->
              permission.name
            end)
          end)

        Enum.uniq(List.flatten(permissions))
    end
  end
end
