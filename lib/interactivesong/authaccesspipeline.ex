defmodule Interactivesong.AuthAccessPipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :interactivesong,
    module: Interactivesong.Guardian,
    error_handler: Interactivesong.AuthErrorHandler,
    key: "interactive_song"

  plug(Guardian.Plug.VerifySession, claims: %{"typ" => "access"})
  plug(Guardian.Plug.VerifyCookie, claims: %{"typ" => "access"})
  plug(Guardian.Plug.LoadResource, allow_blank: true)
  plug(Interactivesong.Context)
end
