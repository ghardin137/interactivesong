defmodule Interactivesong.Role do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "roles" do
    field(:name, :string)
    field(:active, :boolean, default: false)

    many_to_many(:users, Interactivesong.User, join_through: "user_roles", on_delete: :delete_all)
    many_to_many(:permissions, Interactivesong.Permission, join_through: "role_permissions", on_delete: :delete_all)

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name, :active])
    |> validate_required([:name, :active])
    |> put_assoc(:users, [])
    |> put_assoc(:permissions, [])
  end

  def add_perm_changeset(role, permission) do
    role
    |> change()
    |> cast(%{}, [])
    |> put_assoc(:permissions, [permission | role.permissions])
  end
end
