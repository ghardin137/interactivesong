defmodule Interactivesong.PhraseVote do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "phrase_votes" do
    field(:inappropriate, :boolean, default: false)
    field(:panning, :integer)
    field(:volume, :integer)

    belongs_to(:song_phrase, Interactivesong.SongPhrase)
    belongs_to(:user, Interactivesong.User)
    timestamps()
  end

  @doc false
  def changeset(piece_vote, attrs) do
    piece_vote
    |> cast(attrs, [:volume, :panning, :inappropriate, :user_id, :song_phrase_id])
    |> validate_required([:volume, :panning, :inappropriate, :user_id, :song_phrase_id])
  end
end
