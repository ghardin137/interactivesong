defmodule Interactivesong.SongPhrase do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "song_phrases" do
    field(:end, :integer)
    field(:start, :integer)
    field(:volume, :integer)
    field(:pan, :integer)

    belongs_to(:asset, Interactivesong.Asset)
    belongs_to(:song, Interactivesong.Song)
    has_many(:votes, Interactivesong.PhraseVote)
    has_many(:modifications, Interactivesong.SongModification)

    timestamps()
  end

  @doc false
  def changeset(song_piece, attrs) do
    song_piece
    |> cast(attrs, [:start, :end, :volume, :pan, :asset_id, :song_id])
    |> validate_required([:start, :end, :volume, :pan, :asset_id, :song_id])
    |> put_assoc(:votes, [])
    |> put_assoc(:modifications, [])
  end

  def update_changeset(song_piece, attrs) do
    song_piece
    |> cast(attrs, [:start, :end, :volume, :pan])
    |> validate_required([:start, :end, :volume, :pan])
    |> put_assoc(:asset, %{id: attrs.asset_id})
    |> put_assoc(:song, %{id: attrs.song_id})
  end
end
