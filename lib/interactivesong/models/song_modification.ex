defmodule Interactivesong.SongModification do
  use Ecto.Schema
  import Ecto.Changeset

  schema "song_modification" do
    field(:end, :integer)
    field(:pan, :integer)
    field(:start, :integer)
    field(:volume, :integer)

    timestamps()
    belongs_to(:song, Interactivesong.Song)
    belongs_to(:song_phrase, Interactivesong.SongPhrase)
  end

  @doc false
  def changeset(song_modification, attrs) do
    song_modification
    |> cast(attrs, [:start, :end, :volume, :pan])
    |> validate_required([:start, :end, :volume, :pan])
    |> put_assoc(:song_phrase, %{id: attrs.song_phrase_id})
    |> put_assoc(:song, %{id: attrs.song_id})
  end
end
