defmodule Interactivesong.Asset do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "assets" do
    field(:bars, :integer)
    field(:instrument, :string)
    field(:tempo, :integer)
    field(:url, :string)
    field(:author, :string)
    field(:time_signature_beats, :integer)
    field(:time_signature_basis, :integer)

    has_many(:song_phrases, Interactivesong.SongPhrase)

    timestamps()
  end

  @doc false
  def changeset(asset, attrs) do
    asset
    |> cast(attrs, [:url, :bars, :instrument, :tempo, :author])
    |> validate_required([:url, :bars, :instrument, :tempo, :author])
  end

  def update_changeset(songs, attrs) do
    songs
    |> cast(attrs, [:instrument, :bars, :tempo, :url, :author])
    |> validate_required([:instrument, :bars, :tempo, :url, :author])
  end
end
