defmodule Interactivesong.Permission do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "permissions" do
    field(:name, :string)
    field(:active, :boolean, default: false)

    many_to_many(:roles, Interactivesong.Role, join_through: "role_permissions", on_delete: :delete_all)

    timestamps()
  end

  @doc false
  def changeset(permission, attrs) do
    permission
    |> cast(attrs, [:name, :active])
    |> validate_required([:name, :active])
    |> put_assoc(:roles, [])
  end
end
