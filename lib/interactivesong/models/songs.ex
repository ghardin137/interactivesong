defmodule Interactivesong.Song do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "songs" do
    field(:active, :boolean, default: false)
    field(:name, :string)
    field(:update_time, :integer)
    field(:tempo, :integer)

    has_many(:phrases, Interactivesong.SongPhrase)
    has_many(:song_modifications, Interactivesong.SongModification)
    has_many(:votes, Interactivesong.Vote)

    timestamps()
  end

  @doc false
  def create_changeset(songs, attrs) do
    songs
    |> cast(attrs, [:name, :active])
    |> validate_required([:name, :active])
    |> put_assoc(:phrases, [])
    |> put_assoc(:song_modifications, [])
    |> put_assoc(:votes, [])
  end

  def update_changeset(songs, attrs) do
    songs
    |> cast(attrs, [:name, :active])
    |> validate_required([:name, :active])
  end

  def active_changeset(songs, attrs) do
    songs
    |> cast(attrs, [:active])
    |> validate_required([:active])
  end
end
