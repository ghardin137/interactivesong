defmodule Interactivesong.Vote do
  use(InteractivesongWeb, :model)

  @derive {Phoenix.Param, key: :id}

  schema "votes" do
    field(:level, :integer)

    belongs_to(:song, Interactivesong.Song)
    belongs_to(:user, Interactivesong.User)
    timestamps()
  end

  @doc false
  def changeset(vote, attrs) do
    vote
    |> cast(attrs, [:level, :song_id, :user_id])
    |> validate_required([:level, :song_id, :user_id])
  end

  def update_changeset(vote, attrs) do
    vote
    |> cast(attrs, [:level])
    |> validate_required([:level])
  end
end
