defmodule Interactivesong.Guardian do
  use Guardian, otp_app: :interactivesong

  def subject_for_token(resource, _claims) do
    {:ok, to_string(resource.id)}
  end

  def resource_from_claims(claims) do
    {:ok, %{id: claims["sub"]}}
  end

  def after_encode_and_sign(resource, claims, token, _options) do
    with {:ok, _result} <-
           Guardian.DB.after_encode_and_sign(resource, claims["typ"], claims, token) do
      {:ok, token}
    end
  end

  def on_verify(claims, token, _options) do
    with {:ok, _result} <- Guardian.DB.on_verify(claims, token) do
      {:ok, claims}
    else
      _error -> {:ok, claims}
    end
  end

  def on_revoke(claims, token, _options) do
    with {:ok, _} <- Guardian.DB.on_revoke(claims, token) do
      {:ok, claims}
    end
  end
end
