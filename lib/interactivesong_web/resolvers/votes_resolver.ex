defmodule Interactivesong.VotesResolver do
  require Logger
  import Ecto.Query
  alias Interactivesong.Repo
  alias Interactivesong.Vote

  def average_score(%{song_id: song_id}, _context) do
    query =
      Vote
      |> where([v], v.song_id == ^song_id)
      |> select([v], avg(v.level))

    average = Repo.one(query)

    case average do
      nil -> {:ok, 0}
      average -> {:ok, average}
    end
  end

  def average_score(_args, %{source: %{id: song_id}}) do
    query =
      Vote
      |> where([v], v.song_id == ^song_id)
      |> select([v], avg(v.level))

    average = Repo.one(query)

    case average do
      nil -> {:ok, 0}
      average -> {:ok, average}
    end
  end

  def votes_by_user(%{user_id: user_id}, _context) do
    votes =
      case Repo.get_by(Vote, user_id: user_id) do
        nil -> []
        votes -> votes
      end

    query =
      Vote
      |> where([v], v.user_id == ^user_id)
      |> select([v], avg(v.level))

    average = Repo.one(query)

    case average do
      nil -> {:ok, %{votes: votes, average: 0}}
      average -> {:ok, %{votes: votes, average: average}}
    end
  end

  def mine(_args, %{context: %{current_user: %{id: id}}}) do
    case Repo.get_by(Song, active: true) do
      nil ->
        {:error, "No active song"}

      song ->
        case Repo.get_by(Vote, user_id: id, song_id: song.id) do
          nil -> {:error, "No vote on the current song"}
          song -> {:ok, song}
        end
    end
  end

  def vote(%{score: score}, %{
        context: %{current_user: current_user}
      }) do
    case Repo.get_by(Song, active: true) do
      nil ->
        {:error, "No active song"}

      song ->
        %Vote{}
        |> Vote.changeset(%{
          user_id: current_user.user_id,
          song_id: song.id,
          level: score
        })
        |> Repo.insert()
    end
  end

  def update(%{id: id, score: score}, _context) do
    case Repo.get(Vote, id) do
      nil ->
        {:error, "Vote does not exist"}

      vote ->
        vote
        |> Vote.update_changeset(%{
          level: score
        })
        |> Repo.update()
    end
  end

  def remove(%{id: id}, _context) do
    Repo.get(Vote, id)
    |> Repo.delete()
  end
end
