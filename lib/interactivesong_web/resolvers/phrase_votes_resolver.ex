defmodule Interactivesong.PhraseVoteResolver do
  require Logger
  import Ecto.Query
  alias Interactivesong.Repo
  alias Interactivesong.PhraseVote
  alias Interactivesong.Song

  def current(_args, %{context: %{current_user: %{id: id}}}) do
    case Repo.get_by(PhraseVote, user_id: id) do
      nil -> {:error, "No active song"}
      song -> {:ok, song}
    end
  end

  def find(%{song_id: song_id, user_id: user_id}, _context) do
    song =
      Repo.get(Song, song_id)
      |> Repo.preload(:phrases)

    phrase_ids = Enum.map(song.phrases, fn phrase -> phrase.id end)

    PhraseVote
    |> where([p], p.song_phrase_id in ^phrase_ids)
    |> where([p], p.user_id == ^user_id)
    |> Repo.all()
  end

  def vote(%{id: phrase_id, vote: %{volume: volume, pan: pan, inappropriate: inappropriate}}, %{
        context: %{current_user: current_user}
      }) do
    %PhraseVote{}
    |> PhraseVote.changeset(%{
      user_id: current_user.id,
      song_phrase_id: phrase_id,
      volume: volume,
      panning: pan,
      inappropriate: inappropriate
    })
    |> Repo.insert()
  end
end
