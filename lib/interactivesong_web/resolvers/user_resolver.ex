defmodule Interactivesong.UserResolver do
  # import Ecto.Query
  require Logger
  alias Interactivesong.Repo
  alias Interactivesong.User
  alias Interactivesong.Role
  alias Interactivesong.Password

  # Definitely think we should at some point look at more
  # atomic permissioning than what i've done here.
  def all(_args, %{context: %{current_user: %{id: _id}}}) do
    {:ok, Repo.all(User)}
  end

  # I suspect this function could be simplified significantly.
  # I don't know if elixir has decorators or anything.
  # But it seems like a lot of this is common functionality we'd want
  # to do in lots of places.
  def find(%{id: id}, %{context: %{current_user: current_user}}) do
    case current_user.id do
      ^id ->
        get_user(id)

      _ ->
        case current_user.role do
          1 -> get_user(id)
          _ -> {:error, "Not Authorized"}
        end
    end
  end

  def find(_args, %{context: %{current_user: %{id: id}}}) do
    get_user(id)
  end

  @spec get_user(any()) :: {:error, <<_::64, _::_*8>>} | {:ok, any()}
  def get_user(id) do
    case Repo.get(User, id) do
      nil -> {:error, "User id #{id} was not found"}
      user -> {:ok, user}
    end
  end

  def get_user_by_email(email) do
    case Repo.get_by(User, email: email) do
      nil -> {:ok, nil}
      _ -> {:error, "Email already exists"}
    end
  end

  def update(%{id: id, user: user_params}, _info) do
    user = Repo.get!(User, id)

    if user do
      changeset = User.update_changeset(user, user_params)
      Repo.update(changeset, [])
    else
      {:error, "User does not exist"}
    end
  end

  def register(%{user: user_params}, _info) do
    exists = get_user_by_email(user_params.email)

    case exists do
      {:ok, nil} -> create_new_user(user_params)
      {:error, error} -> {:error, error}
    end
  end

  def create_new_user(user_params) do
    new_user = Map.merge(%{password: Password.generate(15)}, user_params)

    user =
      %User{}
      |> User.registration_changeset(new_user)
      |> Repo.insert_or_update()

    case user do
      {:error, error} -> {:error, error}
      _ -> login(%{login: new_user}, nil)
    end
  end

  def login(%{login: params}, _info) do
    case Interactivesong.Session.authenticate(params, Repo) do
      {:ok, user} -> create_session(user)
      {:error, error} -> {:error, error}
    end
  end

  def create_session(user) do
    # with {:ok, token, _} <-
    #        Interactivesong.Guardian.encode_and_sign(user, %{}, token_type: "access") do
    #   user = Map.put(user, :auth_token, token)
    {:ok, user}
    # end
  end

  @spec reset_password(%{confirm: any(), password: any(), token: any()}, any()) ::
          {:error, <<_::104, _::_*24>>} | {:ok, true}
  def reset_password(%{token: token, password: password, confirm: confirm}, _info) do
    # figure out how to handle resetting password here
    if password == confirm do
      case Repo.get_by(ResetToken, token: token) do
        nil -> {:error, "Invalid token"}
        reset_token -> reset_user_password(reset_token, password)
      end
    else
      {:error, "Passwords do not match"}
    end
  end

  def reset_user_password(%{user_id: user_id}, password) do
    result =
      get_user(user_id)
      |> update_user_password(password)

    case result do
      {:error, _} -> {:error, "Invalid Password"}
      {:ok, _} -> {:ok, true}
    end
  end

  def update_user_password({:ok, user}, password) do
    User.update_changeset(user, %{password: password})
    |> Repo.update()
  end

  def update_user_password({:error, error}, _), do: {:error, error}

  def logout(_args, _info) do
    {:ok, true}
  end

  def add_role(%{user_id: user_id, role_id: role_id}, _context) do
    case Repo.get(User, user_id) do
      nil -> {:error, "User does not exist"}
      user -> add_user_role(user, role_id)
    end
  end

  def add_user_role(user, role_id) do
    role =
      Repo.get(Role, role_id)
      |> Repo.preload(:users)

    if role == nil do
      {:error, "role does not exist"}
    else
      user
      |> Repo.preload(:roles)
      |> User.add_role_changeset(role)
      |> Repo.update!()

      user
      |> Repo.preload(:roles)

      {:ok, user}
    end
  end
end
