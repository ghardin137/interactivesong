defmodule Interactivesong.AssetResolver do
  require Logger
  import Ecto.Query
  alias Interactivesong.Repo
  alias Interactivesong.Asset
  alias Interactivesong.SongPhrase

  def current(_args, %{source: %{id: song_id}}) do
    asset_ids =
      SongPhrase
      |> where([sp], sp.song_id == ^song_id)
      |> select([sp], sp.asset_id)
      |> distinct(true)
      |> Repo.all()

    query =
      SongPhrase
      |> where([p], p.song_id == ^song_id)
      |> order_by([p], p.start)

    assets =
      Asset
      |> where([a], a.id in ^asset_ids)
      |> preload(song_phrases: ^query)
      |> Repo.all()

    case assets do
      nil -> {:ok, []}
      assets -> {:ok, assets}
    end
  end

  def all(_args, _context) do
    {:ok, Repo.all(Asset)}
  end

  def find(%{asset_id: asset_id}, _context) do
    case Repo.get(Asset, asset_id) do
      nil -> {:error, "No active song"}
      asset -> {:ok, asset}
    end
  end

  def add(%{asset: asset}, _context) do
    %Asset{}
    |> Asset.changeset(asset)
    |> Repo.insert()
  end

  def update(%{asset_id: asset_id, asset: asset_params}, _context) do
    Repo.get(Asset, asset_id)
    |> Asset.update_changeset(asset_params)
    |> Repo.update()
  end

  def remove(%{asset_id: asset_id}, _context) do
    Repo.get(Asset, asset_id)
    |> Repo.delete()
  end
end
