defmodule Interactivesong.SongResolver do
  require Logger
  alias Interactivesong.Repo
  alias Interactivesong.Song

  def current(_args, _context) do
    case Repo.get_by(Song, active: true) do
      nil -> {:error, "No active song"}
      song -> {:ok, song}
    end
  end

  def new(%{song: %{name: name}}, _context) do
    %Song{}
    |> Song.create_changeset(%{name: name, active: true})
    |> Repo.insert()
  end

  def update(%{song_id: song_id, song: song_params}, _context) do
    Repo.get(Song, song_id)
    |> Song.update_changeset(song_params)
    |> Repo.update()
  end

  def activate(%{song_id: song_id}, _context) do
    Repo.get(Song, song_id)
    |> Song.active_changeset(%{active: true})
    |> Repo.update()
  end

  def deactivate(%{song_id: song_id}, _context) do
    Repo.get(Song, song_id)
    |> Song.active_changeset(%{active: false})
    |> Repo.update()
  end

  def remove(%{song_id: song_id}, _context) do
    Repo.get(Song, song_id)
    |> Repo.delete()
  end
end
