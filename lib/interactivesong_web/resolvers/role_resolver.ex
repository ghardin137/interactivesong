defmodule Interactivesong.RoleResolver do
  require Logger
  alias Interactivesong.Repo
  alias Interactivesong.Role
  alias Interactivesong.Permission

  def all_roles(_args, _context) do
    {:ok, Repo.all(Role)}
  end

  def find_role(%{role_id: role_id}, _context) do
    case Repo.get(Role, role_id) do
      nil -> {:error, "Role does not exist"}
      role -> {:ok, role}
    end
  end

  def all_permissions(_args, _context) do
    {:ok, Repo.all(Permission)}
  end

  def find_permission(%{permission_id: permission_id}, _context) do
    case Repo.get(Permission, permission_id) do
      nil -> {:error, "Permission does not exist"}
      permission -> {:ok, permission}
    end
  end

  def create_role(%{role: role}, _context) do
    %Role{}
    |> Role.changeset(role)
    |> Repo.insert()
  end

  # def update(%{role_id: role_id, role: role}, _context) do
  # end

  def create_permission(%{permission: permission}, _context) do
    %Permission{}
    |> Permission.changeset(permission)
    |> Repo.insert()
  end

  def add_role_permission(%{role_id: role_id, permission_id: permission_id}, _context) do
    case Repo.get(Role, role_id) do
      nil -> {:error, "Role does not exist"}
      role -> add_permission(role, permission_id)
    end
  end

  def add_permission(role, permission_id) do
    permission =
      Repo.get(Permission, permission_id)
      |> Repo.preload(:roles)

    if permission == nil do
      {:error, "permission does not exist"}
    else
      role
      |> Repo.preload(:permissions)
      |> Role.add_perm_changeset(permission)
      |> Repo.update!()

      role
      |> Repo.preload(:permissions)

      {:ok, role}
    end
  end
end
