defmodule Interactivesong.SongPhraseResolver do
  require Logger
  import Ecto.Query
  alias Interactivesong.Repo
  alias Interactivesong.SongPhrase
  alias Interactivesong.Song

  def get_length(phrases) do
    phrases
    |> Enum.map(fn x -> x.end end)
    |> Enum.max_by(fn x -> x end, fn -> 0 end)
  end

  def consolidate_phrases(nil), do: {:ok, []}
  def consolidate_phrases([]), do: {:ok, []}

  def consolidate_phrases(phrases) do
    length = get_length(phrases)

    ticks = List.duplicate([], length)

    ticks =
      Enum.reduce(
        phrases,
        ticks,
        fn x, acc ->
          List.update_at(acc, x.start, fn list ->
            [x | list]
          end)
        end
      )

    {:ok, ticks}
  end

  @spec current(any(), %{source: %{id: any()}}) :: {:ok, any()}
  def current(_args, %{source: %{id: song_id}}) do
    song_phrases =
      SongPhrase
      |> where([sp], sp.song_id == ^song_id)
      |> order_by([sp], sp.start)
      |> Repo.all()

    case song_phrases do
      nil -> {:ok, []}
      song_phrases -> consolidate_phrases(song_phrases)
    end
  end

  def current_length(_args, %{source: %{id: song_id}}) do
    length =
      SongPhrase
      |> where([sp], sp.song_id == ^song_id)
      |> order_by([sp], sp.start)
      |> Repo.all()
      |> get_length

    {:ok, length}
  end

  def all(%{song_id: song_id}, _context) do
    song =
      Repo.get(Song, song_id)
      |> Repo.preload(:phrases)

    {:ok, song.phrases}
  end

  def find(%{song_phrase_id: song_phrase_id}, _context) do
    case Repo.get(SongPhrase, song_phrase_id) do
      nil -> {:error, "No Such Phrase"}
      phrase -> {:ok, phrase}
    end
  end

  def by_asset(_args, context) do
    IO.inspect(context)
    {:ok, []}
  end

  def create(%{song_phrase: phrase}, _context) do
    %SongPhrase{}
    |> SongPhrase.changeset(phrase)
    |> Repo.insert()
  end

  def update(%{song_phrase_id: song_phrase_id, song_phrase: song_phrase}, _context) do
    Repo.get(SongPhrase, song_phrase_id)
    |> SongPhrase.update_changeset(song_phrase)
    |> Repo.update()
  end

  def remove(%{song_phrase_id: song_phrase_id}, _context) do
    Repo.get(SongPhrase, song_phrase_id)
    |> Repo.delete()
  end

  def remove_bulk(%{song_phrase_ids: song_phrase_ids}, _context) do
    SongPhrase
    |> where([sp], sp.id in ^song_phrase_ids)
    |> Repo.delete_all()
  end
end
