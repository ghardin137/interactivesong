defmodule Interactivesong.Schema do
  use Absinthe.Schema
  import_types(Interactivesong.Schema.Types)

  query do
    import_fields(:user_queries)

    import_fields(:role_queries)

    import_fields(:song_queries)

    import_fields(:song_phrases_queries)

    import_fields(:phrase_vote_queries)

    import_fields(:vote_queries)

    import_fields(:asset_queries)
  end

  mutation do
    import_fields(:user_mutations)

    import_fields(:role_mutations)

    import_fields(:song_mutations)

    import_fields(:song_phrases_mutations)

    import_fields(:phrase_vote_mutations)

    import_fields(:asset_mutations)

    import_fields(:vote_mutations)
  end
end
