defmodule Interactivesong.Schema.Types.Songs do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :song do
    field(:id, :id)
    field(:active, :boolean)
    field(:name, :string)
    field(:tempo, :integer)
    field(:update_time, :integer)

    field :length, type: :integer do
      resolve(&Interactivesong.SongPhraseResolver.current_length/2)
    end

    field :phrases, type: list_of(list_of(:song_phrase)) do
      resolve(&Interactivesong.SongPhraseResolver.current/2)
    end

    field :assets, type: list_of(:asset) do
      resolve(&Interactivesong.AssetResolver.current/2)
    end

    field :average_score, type: :float do
      resolve(&Interactivesong.VotesResolver.average_score/2)
    end

    field(:votes, list_of(:vote), resolve: assoc(:votes))

    field :modifications, type: list_of(:song_modification) do
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.create")
    end
  end

  input_object :new_song do
    field(:name, non_null(:string))
    field(:update_time, :integer)
  end

  object :song_queries do
    field :current_song, type: :song do
      resolve(&Interactivesong.SongResolver.current/2)
    end
  end

  object :song_mutations do
    field :create_song, :song do
      arg(:song, non_null(:new_song))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.create")
      resolve(&Interactivesong.SongResolver.new/2)
    end

    field :update_song, :song do
      arg(:song_id, non_null(:string))
      arg(:song, non_null(:new_song))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.update")
      resolve(&Interactivesong.SongResolver.update/2)
    end

    field :deactivate_song, :song do
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.update")
      resolve(&Interactivesong.SongResolver.deactivate/2)
    end

    field :activate_song, :song do
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.update")
      resolve(&Interactivesong.SongResolver.activate/2)
    end

    field :remove_song, :song do
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song.remove")
      resolve(&Interactivesong.SongResolver.remove/2)
    end
  end
end
