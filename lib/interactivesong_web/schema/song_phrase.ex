defmodule Interactivesong.Schema.Types.SongPhrases do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)
  import_types(Interactivesong.Schema.Types.Assets)

  object :song_phrase do
    field(:id, :id)
    field(:end, :integer)
    field(:start, :integer)
    field(:volume, :integer)
    field(:pan, :integer)
    field(:asset, :asset, resolve: assoc(:asset))
    field(:song, :song, resolve: assoc(:song))

    field :votes, list_of(:phrase_vote) do
      resolve(&Interactivesong.PhraseVoteResolver.current/2)
    end
  end

  input_object :new_song_phrase do
    field(:name, non_null(:string))
    field(:end, non_null(:integer))
    field(:start, non_null(:integer))
    field(:volume, non_null(:integer))
    field(:pan, non_null(:integer))
    field(:asset_id, non_null(:string))
    field(:song_id, non_null(:string))
  end

  object :song_phrases_queries do
    field :all_song_phrases, type: list_of(:song_phrase) do
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.update")
      resolve(&Interactivesong.SongPhraseResolver.all/2)
    end

    field :song_phrase, type: :song_phrase do
      arg(:song_phrase_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.update")
      resolve(&Interactivesong.SongPhraseResolver.find/2)
    end
  end

  object :song_phrases_mutations do
    field :create_song_phrase, :song_phrase do
      arg(:song_phrase, non_null(:new_song_phrase))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.create")
      resolve(&Interactivesong.SongPhraseResolver.create/2)
    end

    field :update_song_phrase, :song_phrase do
      arg(:song_phrase_id, non_null(:string))
      arg(:song_phrase, non_null(:new_song_phrase))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.update")
      resolve(&Interactivesong.SongPhraseResolver.update/2)
    end

    field :remove_song_phrase, :song_phrase do
      arg(:song_phrase_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.remove")
      resolve(&Interactivesong.SongPhraseResolver.remove/2)
    end

    field(:remove_song_phrases, :boolean) do
      arg(:song_phrase_ids, non_null(list_of(:string)))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.remove")
      resolve(&Interactivesong.SongPhraseResolver.remove_bulk/2)
    end
  end
end
