defmodule Interactivesong.Schema.Types do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)
  import_types(Interactivesong.Schema.Types.Users)
  import_types(Interactivesong.Schema.Types.Votes)
  import_types(Interactivesong.Schema.Types.SongPhrases)
  import_types(Interactivesong.Schema.Types.PhraseVotes)
  import_types(Interactivesong.Schema.Types.SongModifications)
  import_types(Interactivesong.Schema.Types.Songs)
end
