defmodule Interactivesong.Schema.Types.Users do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)
  import_types(Interactivesong.Schema.Types.Roles)

  object :user do
    field(:id, :id)
    field(:name, :string)
    field(:email, :string)
    field(:avatar, :string)
    field(:votes, list_of(:vote), resolve: assoc(:votes))
    field(:phrase_votes, list_of(:phrase_vote), resolve: assoc(:phrase_votes))
    field(:roles, list_of(:role), resolve: assoc(:roles))
  end

  object :session do
    field(:token, :string)
  end

  input_object :register_user_params do
    field(:email, :string)
  end

  input_object :update_user_params do
    field(:name, :string)
    field(:email, :string)
    field(:password, :string)
  end

  input_object :login_params do
    field(:email, :string)
    field(:password, :string)
  end

  object :user_queries do
    field :users, list_of(:user) do
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.read")
      resolve(&Interactivesong.UserResolver.all/2)
    end

    field :user, type: :user do
      arg(:id, non_null(:id))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.read")
      resolve(&Interactivesong.UserResolver.find/2)
    end

    field :me, type: :user do
      middleware(Interactivesong.Schema.IsAuthenticated)
      resolve(&Interactivesong.UserResolver.find/2)
    end
  end

  object :user_mutations do
    field :login, type: :user do
      arg(:login, non_null(:login_params))
      middleware(Interactivesong.Schema.IsUnauthenticated)
      resolve(&Interactivesong.UserResolver.login/2)
      middleware(Interactivesong.Schema.SetCookie)
    end

    field :logout, type: :boolean do
      middleware(Interactivesong.Schema.IsAuthenticated)
      resolve(&Interactivesong.UserResolver.logout/2)
      middleware(Interactivesong.Schema.ClearCookie)
    end

    field :register_user, type: :user do
      arg(:user, non_null(:register_user_params))
      middleware(Interactivesong.Schema.IsUnauthenticated)
      resolve(&Interactivesong.UserResolver.register/2)
      middleware(Interactivesong.Schema.SetCookie)
    end

    field :update_user, type: :user do
      arg(:id, non_null(:string))
      arg(:user, :update_user_params)
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.update")
      resolve(&Interactivesong.UserResolver.update/2)
    end

    field :remove_user, type: :boolean do
      arg(:id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.remove")
    end

    field :add_user_role, type: :user do
      arg(:user_id, non_null(:string))
      arg(:role_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.role.create")
      resolve(&Interactivesong.UserResolver.add_role/2)
    end

    field :remove_user_role, type: :user do
      arg(:user_id, non_null(:string))
      arg(:role_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "users.role.remove")
      resolve(&Interactivesong.UserResolver.add_role/2)
    end
  end
end
