defmodule Interactivesong.Schema.Types.Assets do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :asset do
    field(:id, :id)
    field(:bars, :integer)
    field(:instrument, :string)
    field(:tempo, :integer)
    field(:time_signature_beats, :integer)
    field(:time_signature_basis, :integer)
    field(:url, :string)
    field(:author, :string)
    field(:song_phrases, type: list_of(:song_phrase))
  end

  input_object :asset_input do
    field(:bars, :integer)
    field(:instrument, :string)
    field(:tempo, :integer)
    field(:time_signature_beats, :integer)
    field(:time_signature_basis, :integer)
    field(:url, :string)
    field(:author, :string)
  end

  object :asset_queries do
    field :all_assets, type: list_of(:asset) do
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.create")
      resolve(&Interactivesong.AssetResolver.all/2)
    end

    field :asset, type: :asset do
      arg(:asset_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "song_phrase.create")
      resolve(&Interactivesong.AssetResolver.find/2)
    end
  end

  object :asset_mutations do
    field :add_asset, type: :asset do
      arg(:asset, non_null(:asset_input))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "assets.create")
      resolve(&Interactivesong.AssetResolver.add/2)
    end

    field :update_asset, type: :asset do
      arg(:asset_id, non_null(:string))
      arg(:asset, non_null(:asset_input))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "assets.update")
      resolve(&Interactivesong.AssetResolver.update/2)
    end

    field :remove_asset, type: :asset do
      arg(:asset_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "assets.remove")
      resolve(&Interactivesong.AssetResolver.remove/2)
    end
  end
end
