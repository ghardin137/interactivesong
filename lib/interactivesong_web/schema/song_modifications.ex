defmodule Interactivesong.Schema.Types.SongModifications do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :song_modification do
    field(:id, :id)
    field(:end, :integer)
    field(:start, :integer)
    field(:volume, :integer)
    field(:pan, :integer)
    field(:song_phrase, :song_phrase, resolve: assoc(:song_phrase))
    field(:song, :song, resolve: assoc(:song))
  end

  object :song_modification_queries do
  end

  object :song_modification_mutations do
  end
end
