defmodule Interactivesong.Schema.Types.Votes do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :vote do
    field(:id, :id)
    field(:level, :integer)
    field(:song, :song, resolve: assoc(:song))
    field(:user, :user, resolve: assoc(:user))
  end

  object :user_votes do
    field(:average, :integer)
    field(:votes, list_of(:vote))
  end

  object :vote_queries do
    field :average_score, :float do
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      resolve(&Interactivesong.VotesResolver.average_score/2)
    end

    field :votes_by_user, :user_votes do
      arg(:user_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      resolve(&Interactivesong.VotesResolver.votes_by_user/2)
    end

    field :my_vote, :vote do
      middleware(Interactivesong.Schema.IsAuthenticated)
      resolve(&Interactivesong.VotesResolver.mine/2)
    end
  end

  object :vote_mutations do
    field :vote_on_song, :vote do
      arg(:score, non_null(:integer))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "vote.create")
      resolve(&Interactivesong.VotesResolver.vote/2)
    end

    field :update_vote_on_song, :vote do
      arg(:id, non_null(:string))
      arg(:score, non_null(:integer))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "vote.update")
      resolve(&Interactivesong.VotesResolver.update/2)
    end

    field :remove_vote_on_song, :vote do
      arg(:id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "vote.remove")
      resolve(&Interactivesong.VotesResolver.remove/2)
    end
  end
end
