defmodule Interactivesong.Schema.Types.PhraseVotes do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :phrase_vote do
    field(:id, :id)
    field(:inappropriate, :boolean)
    field(:panning, :integer)
    field(:volume, :integer)
    field(:song_phrase, :song_phrase, resolve: assoc(:song_phrase))
    field(:user, :user, resolve: assoc(:user))
  end

  input_object :phrase_vote_input do
    field(:volume, :integer)
    field(:pan, :integer)
    field(:inappropriate, :boolean)
  end

  object :user_phrase_votes do
    field(:user, :user, resolve: assoc(:user))
    field(:average, :integer)
    field(:phrase_votes, list_of(:phrase_vote))
  end

  object :phrase_vote_queries do
    field :phrase_votes_by_user, :user_phrase_votes do
      arg(:user_id, non_null(:string))
      arg(:song_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "phrase_vote.read")
      resolve(&Interactivesong.PhraseVoteResolver.find/2)
    end
  end

  object :phrase_vote_mutations do
    field :vote_on_phrase, type: :phrase_vote do
      arg(:id, non_null(:string))
      arg(:vote, non_null(:phrase_vote_input))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "phrase_vote.create")
      resolve(&Interactivesong.PhraseVoteResolver.vote/2)
    end

    field(:update_vote_on_phrase, :phrase_vote) do
      arg(:phrase_id, non_null(:string))
      arg(:phrase_vote, non_null(:phrase_vote_input))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "phrase_vote.update")
    end

    field(:cancel_vote_on_phrase, :phrase_vote) do
      arg(:phrase_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "phrase_vote.update")
    end

    field(:remove_phrase_votes, :boolean) do
      arg(:phrase_ids, non_null(list_of(:string)))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "phrase_vote.remove")
    end
  end
end
