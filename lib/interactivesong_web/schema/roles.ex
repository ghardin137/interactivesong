defmodule Interactivesong.Schema.Types.Roles do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Interactivesong.Repo
  import_types(Absinthe.Type.Custom)

  object :role do
    field(:id, :id)
    field(:name, :string)
    field(:active, :boolean)
    field(:users, list_of(:user), resolve: assoc(:users))
    field(:permissions, list_of(:permission), resolve: assoc(:permissions))
  end

  object :permission do
    field(:id, :id)
    field(:name, :string)
    field(:active, :boolean)
    field(:roles, list_of(:role), resolve: assoc(:roles))
  end

  input_object :new_role do
    field(:name, :string)
    field(:active, :boolean)
  end

  input_object :new_permission do
    field(:name, :string)
    field(:active, :boolean)
  end

  input_object :add_role_perm do
    field(:role_id, :id)
    field(:perm_id, :id)
  end

  object :role_queries do
    field :roles, list_of(:role) do
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.read")
      resolve(&Interactivesong.RoleResolver.all_roles/2)
    end

    field :role, type: :role do
      arg(:role_id, non_null(:id))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.read")
      resolve(&Interactivesong.RoleResolver.find_role/2)
    end

    field :permissions, list_of(:permission) do
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "permission.read")
      resolve(&Interactivesong.RoleResolver.all_permissions/2)
    end

    field :permission, type: :permission do
      arg(:permission_id, non_null(:id))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "permission.read")
      resolve(&Interactivesong.RoleResolver.find_permission/2)
    end
  end

  object :role_mutations do
    field :add_role, type: :role do
      arg(:role, non_null(:new_role))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.create")
      resolve(&Interactivesong.RoleResolver.create_role/2)
    end

    field :update_role, type: :role do
      arg(:role_id, non_null(:string))
      arg(:role, non_null(:new_role))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.update")
      resolve(&Interactivesong.RoleResolver.create_role/2)
    end

    field :remove_role, type: :role do
      arg(:role_id, non_null(:string))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.remove")
      resolve(&Interactivesong.RoleResolver.create_role/2)
    end

    field :add_permission, type: :permission do
      arg(:permission, non_null(:new_permission))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "permission.create")
      resolve(&Interactivesong.RoleResolver.create_permission/2)
    end

    field :add_role_permission, type: :role do
      arg(:role_id, non_null(:id))
      arg(:permission_id, non_null(:id))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.permission.create")
      resolve(&Interactivesong.RoleResolver.add_role_permission/2)
    end

    field :remove_role_permission, type: :role do
      arg(:role_id, non_null(:id))
      arg(:permission_id, non_null(:id))
      middleware(Interactivesong.Schema.IsAuthenticated)
      middleware(Interactivesong.Schema.HasPermission, "role.permission.remove")
      resolve(&Interactivesong.RoleResolver.add_role_permission/2)
    end
  end
end
