defmodule Interactivesong.Schema.IsUnauthenticated do
  @behaviour Absinthe.Middleware

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: %{id: _}} ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Not Authorized"})

      _ ->
        resolution
    end
  end
end
