defmodule Interactivesong.Schema.HasPermission do
  @behaviour Absinthe.Middleware

  def call(resolution, permission) do
    case resolution.state do
      :resolved ->
        resolution

      _ ->
        case Enum.member?(resolution.context.current_user.roles, permission) do
          true ->
            resolution

          _ ->
            resolution
            |> Absinthe.Resolution.put_result({:error, "Not Authorized"})
        end
    end
  end
end
