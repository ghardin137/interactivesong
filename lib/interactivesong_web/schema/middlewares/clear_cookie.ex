defmodule Interactivesong.Schema.ClearCookie do
  @behaviour Absinthe.Middleware

  def call(resolution, _config) do
    case resolution.state do
      :resolved ->
        Map.update!(resolution, :context, fn ctx ->
          Map.put(ctx, :clear_auth, true)
        end)
    end
  end
end
