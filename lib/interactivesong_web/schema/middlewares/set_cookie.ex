defmodule Interactivesong.Schema.SetCookie do
  @behaviour Absinthe.Middleware

  def call(resolution, _config) do
    case resolution.state do
      :resolved ->
        with %{value: user} <- resolution do
          resolution =
            Map.update!(resolution, :context, fn ctx ->
              Map.put(ctx, :auth_user, user)
            end)

          resolution
        end
    end
  end
end
