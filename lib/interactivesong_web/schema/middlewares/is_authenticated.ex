defmodule Interactivesong.Schema.IsAuthenticated do
  @behaviour Absinthe.Middleware

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: %{id: _}} ->
        resolution

      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Not Authorized"})
    end
  end
end
