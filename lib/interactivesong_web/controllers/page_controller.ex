defmodule InteractivesongWeb.PageController do
  use InteractivesongWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
