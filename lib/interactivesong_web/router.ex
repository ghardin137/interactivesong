defmodule InteractivesongWeb.Router do
  use InteractivesongWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :auth do
    plug(Interactivesong.AuthAccessPipeline)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/" do
    pipe_through(:api)
    pipe_through(:auth)

    forward("/api", Absinthe.Plug,
      schema: Interactivesong.Schema,
      before_send: {__MODULE__, :absinthe_before_send}
    )

    forward("/graphql", Absinthe.Plug.GraphiQL,
      schema: Interactivesong.Schema,
      before_send: {__MODULE__, :absinthe_before_send}
    )
  end

  scope "/", InteractivesongWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/*path", PageController, :index)
  end

  # Other scopes may use custom stacks.
  # scope "/api", InteractivesongWeb do
  #   pipe_through :api
  # end

  def absinthe_before_send(conn, %Absinthe.Blueprint{} = blueprint) do
    if auth_user = blueprint.execution.context[:auth_user] do
      Interactivesong.Guardian.Plug.sign_in(conn, auth_user)
      |> Interactivesong.Guardian.Plug.remember_me(auth_user)
    else
      if blueprint.execution.context[:clear_auth] do
        conn
        |> Interactivesong.Guardian.Plug.sign_out()
        |> Interactivesong.Guardian.Plug.clear_remember_me()
      else
        conn
      end
    end
  end

  def absinthe_before_send(conn, _) do
    conn
  end
end
